/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package org.slingerxv.limitart.i18n;

/**
 * 语言枚举
 * 
 * @author hank
 *
 */
public enum ISOLanguages {
	/**
	 * 保加利亚语
	 */
	bg,
	/**
	 * 中文(中国台湾)
	 */
	zh_tw,
	/**
	 * 中文(中华人民共和国)
	 */
	zh_cn,
	/**
	 * 克罗地亚语
	 */
	hr,
	/**
	 * 捷克语
	 */
	cs,
	/**
	 * 丹麦语
	 */
	da,
	/**
	 * 荷兰语(标准)
	 */
	nl,
	/**
	 * 英语
	 */
	en,
	/**
	 * 波斯语
	 */
	fa,
	/**
	 * 法语(标准)
	 */
	fr,
	/**
	 * 德语(标准)
	 */
	de,
	/**
	 * 希腊语
	 */
	el,
	/**
	 * 匈牙利语
	 */
	hu,
	/**
	 * 冰岛语
	 */
	is,
	/**
	 * 印度尼西亚语
	 */
	in,
	/**
	 * 意大利语(标准)
	 */
	it,
	/**
	 * 日语
	 */
	ja,
	/**
	 * 韩语
	 */
	ko,
	/**
	 * 马来西亚语
	 */
	ms,
	/**
	 * 挪威语
	 */
	no,

	/**
	 * 波兰语
	 */
	pl,
	/**
	 * 葡萄牙语
	 */
	pt,
	/**
	 * 俄语
	 */
	ru,
	/**
	 * 西班牙语(西班牙传统)
	 */
	es,
	/**
	 * 泰语
	 */
	th,
	/**
	 * 土耳其语
	 */
	tr,
	/**
	 * 乌克兰语
	 */
	uk,
	/**
	 * 越南语
	 */
	vi,;
}
